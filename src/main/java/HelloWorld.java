import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class HelloWorld {

    /*
    Given an array of integers e, arrange the elements in it so that:

e[0] ≤ e[1] ≥ e[2] ≤ e[3] ≥ e[4] ...

The elements in e need not be unique (they may be repeated)



     */

    /*
    // 1 , mid + 1 mid /
    // 1 2 5 7 8 9
    // 1 < 9 > 2 8 5 7

    /// 1 2 5 7 8 9
    // 1 2 5 7 8 9
    //


    // 9 8 7 5 2 1

    // 1 ≤ 7 ≥ 5
    // ≤ 9 ≥ 2 ≤ 8
    5 2 1 7 9 8 ->  1 ≤ 7 ≥ 5 ≤ 9 ≥ 2 ≤ 8  or  2 ≤ 5 ≥ 1 ≤ 9 ≥ 7 ≤ 8 or ...

1 2 3 4 5 6 ->  1 ≤ 3 ≥ 2 ≤ 5 ≥ 4 ≤ 6  or ...

-2 3 3 -3   ->  3 ≤ 3 ≥ -3 ≤ -2  or  -2 ≤ 3 ≥ -3 ≤ 3 or ...

     */

    // 5 2 1 7 9 8
    // 1 2 5 7 8 9

    //  i + 1 / i + 2/ i
    // i + 1 i - 1
    //
    // 2 < 5 > 1 < 9

    public static int[] rearange(int arr[]) {
        Arrays.sort(arr);

        var res = new int[arr.length];
        var begin = true;
        var start = 0;
        var end = arr.length - 1;


        for (var i = 0; i < arr.length; i++) {

            if(begin) {
                res[i] = arr[start++];
            } else {
                res[i] = arr[end --];
            }

            begin = !begin;
        }

        return res;
    }



    public static Boolean hasSubArr(int arr[]) {

        var set = new HashSet<Integer>();
        int sum = 0;

        for(var i = 0; i < arr.length; i++) {
            sum+= arr[i];

            if(arr[i] == 0 ||
                    sum == 0 ||
                    set.contains(sum)
            ) {
                return true;
            }

            set.add(sum);
        }

        return false;
    }


    public static Character firstRepeatedCharacter(String value) {
        var existing = new HashMap<Character, Integer>();

        var lowest = value.length();
        for(var i = 0; i < value.length(); i++) {

            char val = value.charAt(i);
            if(existing.containsKey(val)) {
                if(existing.get(val) < lowest) {
                    lowest = existing.get(val);
                }
            } else {
                existing.put(val, i);
            }

        }

        return lowest < value.length() ? value.charAt(lowest) : null;
    }

}
