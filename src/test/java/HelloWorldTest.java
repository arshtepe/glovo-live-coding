import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class HelloWorldTest {

    @Test
    public void testRearange() {
        int[] tCase1 = {1,2, 3, 4, 5, 6};
        int[] tCase2 = {5, 2, 1, 7, 9, 8};

        testSub(tCase1);
        testSub(tCase2);
    }

    public void testSub(int arr[]) {
        var e = HelloWorld.rearange(arr);
        var greater = true;

        System.out.println( Arrays.toString(e));

        for(var i = 1; i < e.length - 1; i++) {
            if(greater) {
                assertThat(e[i] >= e[i - 1]).isTrue();
            } else  {
                assertThat( e[i] <=  e[i - 1]).isTrue();

            }

            greater = !greater;
        }

    }




    @Test
    public void testSub() {
        int[] tCase1 = {4, 2, -3, 1, 6};
        int[] tCase2 = {4, 2, 0, 1, 6};
        int[] tCase3 = {-3, 2, 3, 1, 6};

        assertThat(HelloWorld.hasSubArr(tCase1)).isTrue();
        assertThat(HelloWorld.hasSubArr(tCase2)).isTrue();
        assertThat(HelloWorld.hasSubArr(tCase3)).isFalse();
    }

    @Test
    public void test() {

        assertThat(HelloWorld.firstRepeatedCharacter("BCABA")).isEqualTo('B');
        assertThat(HelloWorld.firstRepeatedCharacter("glovol")).isEqualTo('l');
        assertThat(HelloWorld.firstRepeatedCharacter("ABC")).isNull();
      assertThat(HelloWorld.firstRepeatedCharacter("ABCA")).isEqualTo('A');
    }
}
